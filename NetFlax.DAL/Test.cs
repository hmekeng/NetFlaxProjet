﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetFlax.DAL
{
    public class Test
    {
        private string Cnstr = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=NetFLax;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private SqlConnection oConn;
        private SqlCommand oCmd;

        public bool Connect()
        {
            try
            {
                oConn = new SqlConnection(Cnstr);
                oConn.Open();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Disconnect()
        {
            try
            {
                oConn.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Insert(String Query)
        {
            if(Connect())
            {
                oCmd = oConn.CreateCommand();
                oCmd.CommandText = Query;
                try
                {
                    oCmd.ExecuteNonQuery();
                    Disconnect();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        public List<Dictionary<string, object>> getData(string Query)
        {
            if(Connect())
            {
                SqlDataReader oDr;
                oCmd = oConn.CreateCommand();
                oCmd.CommandText = Query;  //Query = "Select * from...."
                  List<Dictionary<string, object>> Enregistrements = new List<Dictionary<string, object>>();
                  try
                {
                    oDr = oCmd.ExecuteReader();
                    while(oDr.Read())
                    {
                        //Pour récupérer soit 
                        // oDr[0] ==> 1er champs en tant qu'objet
                        //oDr["id"]==> Champs colonne id en tant qu'objet
                        //oDr.GetInt32(0)==> Premier champs en tant qu'Int
                        // string NomCol = oDr.GetName(0);==> nom de la première colonne
                        //int nbChamps = oDr.FieldCount;==> Nombre de colonne
                        //oDr.HasRows ==> Y'a-t-il des résultats ?
                        Dictionary<string, object> Row = new Dictionary<string, object>();
                        for (int i = 0; i < oDr.FieldCount; i++)
                        {
                            string Key =oDr.GetName(i);
                            object Value = oDr[i];
                            Row.Add(Key, Value);

                        }
                        Enregistrements.Add(Row);


                    }
                    oDr.Close();
                    Disconnect();
                }
                catch (Exception)
                {

                    //throw;
                }


                return Enregistrements;
            }
            return null;
        }
    }
}
