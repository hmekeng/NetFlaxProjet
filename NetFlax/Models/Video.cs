﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetFlax.Models
{
    public class Video
    {
        private int _id;
        private string _thumb;
        private string _title;
        private string _shortDesc;
        private bool _isLast = false;
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public string ShortDesc
        {
            get { return _shortDesc; }
            set { _shortDesc = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string Thumb
        {
            get { return _thumb; }
            set { _thumb = value; }
        }

        public bool IsLast { get => _isLast; set => _isLast = value; }
    }
}