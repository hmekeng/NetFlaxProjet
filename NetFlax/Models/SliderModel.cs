﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetFlax.Models
{
    public class SliderModel
    {
        private string _id;
        private List<string> _pictures;

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        //Nouveauté C# 7
        public List<string> Pictures
        { get => _pictures;
            set => _pictures = value;
        }
    }
}