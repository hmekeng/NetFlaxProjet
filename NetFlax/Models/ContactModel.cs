﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NetFlax.Models
{
    public class ContactModel
    {
        private string _name;
        private string _email;
        private string _phone;
        private string _message;

        [Required]
        public string Name { get => _name; set => _name = value; }

        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress,
            ErrorMessage ="Apprend a écrire")]
        public string Email { get => _email; set => _email = value; }

        [RegularExpression(@"^.{3,}$", 
            ErrorMessage = "Minimum 3 characters required")]
        public string Phone { get => _phone; set => _phone = value; }


        [Required]
        public string Message { get => _message; set => _message = value; }

       
    }
}