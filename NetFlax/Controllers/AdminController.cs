﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetFlax.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Contact()
        {
            //1- Instancier ma class test
            DAL.Test db = new DAL.Test();
            string Query = "Select * from Contact";
            List<Dictionary<string, object>> infos = db.getData(Query);


            return View(infos);
        }

        public ActionResult Customer()
        {
            //1- Instancier ma class test
            DAL.Test db = new DAL.Test();
            string Query = "Select * from Customer";
            List<Dictionary<string, object>> infos = db.getData(Query);


            return View(infos);
        }

        public ActionResult Media()
        { //1- Instancier ma class test
            DAL.Test db = new DAL.Test();
            string Query = "Select * from Media";
            List<Dictionary<string, object>> infos = db.getData(Query);


            return View(infos);
        }
    }
}