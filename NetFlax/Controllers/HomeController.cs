﻿using NetFlax.DAL;
using NetFlax.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace NetFlax.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            SliderModel SM = new SliderModel();
            SM.Id = "flexiselDemo1";
            SM.Pictures = new List<string>();
            SM.Pictures.Add("r1.jpg");
            SM.Pictures.Add("r2.jpg");
            SM.Pictures.Add("r3.jpg");
            SM.Pictures.Add("r4.jpg");
            SM.Pictures.Add("r5.jpg");
            SM.Pictures.Add("r6.jpg");

            SliderModel SM2 = new SliderModel();
            SM2.Id = "flexiselDemo2";
            SM2.Pictures = new List<string>();
            SM2.Pictures.Add("m1.jpg");
            SM2.Pictures.Add("m2.jpg");
            SM2.Pictures.Add("m3.jpg");
            SM2.Pictures.Add("m4.jpg");

            List<SliderModel> LSM = new List<SliderModel>();
            LSM.Add(SM);
            LSM.Add(SM2);

            return View(LSM);
        }

        public ActionResult Video()
        {
            List<string> images = new List<string> { "gridallbum1.jpg", "gridallbum2.jpg", "gridallbum3.jpg", "gridallbum4.jpg", "gridallbum5.jpg", "gridallbum6.jpg", "gridallbum7.jpg", "gridallbum8.jpg", "gridallbum9.jpg" };
            string Desc = "Lorem ipsum dolor sit amet, consectetur adipisicing elit.";
            List<Models.Video> LV = new List<Models.Video>();
            int cpt = 1;
            foreach (string item in images)
            {                
                Models.Video v =new Models.Video() { ID = cpt, ShortDesc = Desc, Thumb = item, Title = item };
                if (cpt % 4 == 0) v.IsLast = true;
                LV.Add(v);
                cpt++;
            }
            return View(LV);
        }
        [HttpGet]
        public ActionResult Contact()
        {
            //1- Instancier ma class test
            DAL.Test db = new DAL.Test();
            string Query = "Select * from Contact";
           List<Dictionary<string,object>> infos = db.getData(Query);


            return View(infos);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactModel CM)
        {
            //1- Instancier ma class test
            DAL.Test db = new DAL.Test();
            string Query = "Select * from Contact";
            List<Dictionary<string, object>> infos = db.getData(Query);

            
            if (ModelState.IsValid)
            {
               // DAL.Test db = new DAL.Test();
                Query = string.Format(
                    @"insert into Contact 
                                (Name,Email, Phone, Message)
                                 VALUES
                                ( '{0}','{1}'   ,'{2}'   ,'{3}')",
                                CM.Name,CM.Email,CM.Phone,CM.Message);

                if (db.Insert(Query))
                {
                    Query = "Select * from Contact";
                    infos = db.getData(Query);
                }
                return View(infos);
            }
            else
            {
                ViewBag.ErrorMessage = "Zut";
                return View(infos);
            }
        }

        public ActionResult reviews()
        {


            return View();
        }
    }
}