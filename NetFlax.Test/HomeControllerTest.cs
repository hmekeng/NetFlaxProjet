﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetFlax.Controllers;
using System.Web.Mvc;
using NetFlax.Models;
using System.Collections.Generic;

namespace NetFlax.Test
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void HomeIndexTest()
        {
            //Arrange 
            HomeController Hm = new HomeController();

            //Act
            ActionResult retour = Hm.Index();
            //Assert

            Assert.IsInstanceOfType(retour, typeof(ViewResult));
            ViewResult vr = retour as ViewResult;
          Assert.AreEqual("", vr.ViewName );

          Assert.IsNotNull(vr.Model  as List<SliderModel>);

           List<SliderModel> LM = vr.Model as List<SliderModel>;

            Assert.AreEqual(2, LM.Count);
            Assert.AreEqual(6, LM[0].Pictures.Count);
            Assert.AreEqual(4, LM[1].Pictures.Count);
        }
    }

   
}
